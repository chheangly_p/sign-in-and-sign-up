//
//  SignUpViewController.swift
//  sign up and log in
//
//  Created by Pathmazing on 8/6/19.
//  Copyright © 2019 Pathmazing Inc. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController {

    @IBOutlet weak var fullNameView: UIView!
    @IBOutlet weak var firstView: UIView!
    @IBOutlet weak var hotelView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var roomNumberView: UIView!
    @IBOutlet weak var birthdayView: UIView!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var fullNameTextField: UITextField!
    @IBOutlet weak var hotelTextField: UITextField!
    @IBOutlet weak var roomNumberTextField: UITextField!
    @IBOutlet weak var birthDayTextField: UITextField!
    @IBOutlet weak var signInButton: UIButton!
    
    var activeView: UIView!
    var keyboardHeight = CGFloat()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTextFieldDelegate()
        addObserver()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    func setTextFieldDelegate() {
        fullNameTextField.delegate = self
        hotelTextField.delegate = self
        roomNumberTextField.delegate = self
        birthDayTextField.delegate = self
    }
    
    func addObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardNotification), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(notification: Notification) {
        var editingTextFieldY = stackView.frame.height / 6
        let sizeForEachRow = stackView.frame.height / 6
        let topSpace = firstView.frame.height
        
        if activeView == fullNameView {
            editingTextFieldY = editingTextFieldY * 1
        } else if activeView == hotelView {
            editingTextFieldY = editingTextFieldY * 2
        } else if activeView == roomNumberView {
            editingTextFieldY = editingTextFieldY * 3
        } else if activeView == birthdayView {
            editingTextFieldY = editingTextFieldY * 4
        }
        
        editingTextFieldY = self.view.frame.height - (editingTextFieldY + topSpace) - (sizeForEachRow - roomNumberTextField.frame.height)
        
        if editingTextFieldY < keyboardHeight {
            scrollView.setContentOffset(CGPoint(x: 0, y: (keyboardHeight - editingTextFieldY) + 6), animated: true)
        }
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }
    
    @objc func keyboardNotification(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            keyboardHeight = keyboardRectangle.height
        }
    }
    
    @IBAction func onSignInButtonClick(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SignInView") as! SignInViewController
        present(vc, animated: true, completion: nil)
    }
    
    @IBAction func onCloseButtonClick(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}

extension SignUpViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == fullNameTextField {
            activeView = fullNameView
        } else if textField == hotelTextField {
            activeView = hotelView
        } else if textField == roomNumberTextField {
            activeView = roomNumberView
        } else if textField == birthDayTextField {
            activeView = birthdayView
        }
        
        return true
    }
    
}

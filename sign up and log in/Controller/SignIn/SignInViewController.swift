//
//  SignInViewController.swift
//  sign up and log in
//
//  Created by Pathmazing on 8/6/19.
//  Copyright © 2019 Pathmazing Inc. All rights reserved.
//

import UIKit

class SignInViewController: UIViewController {

    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var mailTextField: UITextField!
    @IBOutlet weak var mailView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var passwordView: UIView!
    
    var activeView: UIView!
    var keyboardHeight = CGFloat()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        setUpTextFieldDelegate()
        addObserver()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    func setUpTextFieldDelegate() {
        passwordTextField.delegate = self
        mailTextField.delegate = self
    }
    
    func addObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardNotification), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(notification: Notification) {
        var editingTextFieldY = self.activeView.frame.origin.y * 2
        
        //Getting Real Y Of 1st Textfield
        if editingTextFieldY == 0 {
            editingTextFieldY = self.view.frame.height / 2
            editingTextFieldY -= (editingTextFieldY * 0.4) / 1.6
        }
        
        if editingTextFieldY < keyboardHeight {
            scrollView.setContentOffset(CGPoint(x: 0, y: (keyboardHeight - editingTextFieldY) + 5), animated: true)
        }
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }
    
    @objc func keyboardNotification(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            keyboardHeight = keyboardRectangle.height
        }
    }
    
    @IBAction func onSignUpButtonClick(_ sender: Any) {
        //SignUpView
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SignUpView") as! SignUpViewController
        present(vc, animated: true)
    }
    
}

extension SignInViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == mailTextField {
            activeView = mailView
        } else if textField == passwordTextField {
            activeView = passwordView
        }
        return true
    }
    
}
